import subprocess
import optparse
import re

def get_arguments():
    parser = optparse.OptionParser()
    parser.add_option("-i", "--interface", dest="target_interface", help="Interface to change its MAC Address.")
    parser.add_option("-m", "--mac", dest="fake_mac_address", help="New MAC Address")
    (options, arguments) = parser.parse_args()

    if not options.target_interface:
        print("[-] Please specify the target's interface, use --help for more info.")
    elif not options.fake_mac_address:
        print("[-] Please specify the MAC Address, use --help for more info.")

    return options, arguments


def get_mac_address(target_interface):
    output_mac = subprocess.check_output(['ifconfig', target_interface])
    print(output_mac)

    filtered_mac_address = re.search(r"\w\w:\w\w:\w\w:\w\w:\w\w:\w\w", output_mac)

    if filtered_mac_address:
        return filtered_mac_address.groups(0)
    else:   
        return "[+] MAC Address not found."


def mac_changer(target_interface, fake_mac_address):
    print(f"[+] Assinging {fake_mac_address} to interface {target_interface}")
    subprocess.call(['ifconfig', target_interface, 'down'])
    subprocess.call(['ifconfig', target_interface, 'hw', 'ether', fake_mac_address])
    subprocess.call(['ifconfig', target_interface, 'up'])
    print("[+] Jobs Done. Congratulations!!!")


(options, arguments) = get_arguments()
target_interface = options.target_interface
fake_mac_address = options.fake_mac_address

current_mac_address = get_mac_address(target_interface)
print(f"[+] The MAC Address is: {current_mac_address}.")

mac_changer(target_interface, fake_mac_address)

new_mac_address = get_mac_address(target_interface)
if new_mac_address == fake_mac_address:
    print(f"[+] The MAC Address was changed to {fake_mac_address}.")
else:
    print("[+] Something went wrong. MAC Address was not changed.")

