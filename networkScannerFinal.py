import scapy.all as scapy
import argparse

def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--target", dest="target", help="Target IP / IP ranges.")
    options = parser.parse_args()
    return options


def scan(target_ip):
    clients_list = []
    arp_request = scapy.ARP(pdst=target_ip)
    broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast/arp_request
    answered_list = scapy.srp(arp_request_broadcast, timeout=1, verbose=False)[0]
    for client in answered_list:
        client_dict = {"ip":client[1].psrc , "mac":client[1].hwsrc}
        clients_list.append(client_dict)
    return clients_list

def print_results(scan_result):
    print(print("IP\t\t\tMAC Address\n--------------------------------------------"))
    for client in scan_result:
        print(client["ip"] + "\t\t" + client["mac"])



options = get_arguments()
target_ip = options.target

scan_result = scan(target_ip)

print_results(scan_result)